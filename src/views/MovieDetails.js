import {useParams} from "react-router-dom";
import {MoviesService} from "../api/MoviesService";
import {useEffect, useState} from "react";

export const MovieDetails = () => {
    const {id} = useParams();

    const [movie, setMovie] = useState([]);
    const fetchMovies = async () => {
        const {data} = await MoviesService.getMovieById(id);
        setMovie(data);
    }
    useEffect(() => {
        fetchMovies();
    }, []);
    return (
        <>
            <h1>{movie.title}</h1>
            <article>{movie.overview}</article>
        </>
    )
};